/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);
var BackgroundGeolocation = window.BackgroundGeolocation
var bgGeo
// const BGGEO_BACKEND_URL = 'https://apibg/v1/'
// const BGGEO_BACKEND_URL = 'https://192.168.1.12/v1/' //Brian server14
const BGGEO_BACKEND_URL = 'https://192.168.1.15/v1/' //Brian laptop
var dest_addr = {lat: 37.358322606629685, lng: -121.89952780335919} //1036 N 6th St, San Jose, CA 95112
const GEOFENCE_METER = 200

var geo_fence_dest
var trip_id

function onDeviceReady() {
  // Cordova is now initialized. Have fun!

  console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
  document.getElementById('deviceready').classList.add('ready');
  // 1.  Listen to events
  bgGeo = window.BackgroundGeolocation;
  bgGeo.onMotionChange(function (event) {
    console.log('[motionchange] -', event.isMoving, event.location);
  });
  bgGeo.onProviderChange(function (event) {
    console.log('[providerchange] -', event.status, event.enabled, event.gps, event.network);
  });

// 2. Execute #ready method:
  bgGeo.ready({
    desiredAccuracy: bgGeo.DESIRED_ACCURACY_HIGH,   // <-- Config params
    distanceFilter: 50,
    debug: true,
    // url: "https://192.168.1.12/v1/tblcommutelog-verified-points",
    // url: "https://apibg/v1/tblcommutelog-verified-points",
    // url: "https://apibg.socalappsolutions.com/v1/tblcommutelog-verified-points",
    // method: "POST",
    autoSync: true,
    logLevel: bgGeo.LOG_LEVEL_VERBOSE,
    logMaxDays: 1 // <-- 1 days of logs
  }, function (state) {    // <-- Current state provided to #configure callback
    // 3.  Start tracking
    console.log('BackgroundGeolocation is configured and ready to use');
    if (!state.enabled) {
      bgGeo.start().then(function () {
        console.log('- BackgroundGeolocation tracking started');
      });
    }
  });

// NOTE:  Do NOT execute any API methods which will access location-services
// until the callback to #ready executes!
//
// For example, DO NOT do this here:
//
// bgGeo.getCurrentPosition();   // <-- NO!
// bgGeo.start();                // <-- NO!
}

async function start_log() {
  console.log('Starting log')
  trip_id = (new Date()).toISOString()
  axios.post(BGGEO_BACKEND_URL + 'tblcommutelog-verified', {
    trip_id: trip_id, id_commuter: 123456, status: 'S'
  })
  await bgGeo.removeGeofence('LogTripDest')
  geo_fence_dest = bgGeo.addGeofence({
    identifier: "LogTripDest",
    radius: GEOFENCE_METER,
    latitude: dest_addr.lat,
    longitude: dest_addr.lng,
    notifyOnEntry: true,
    notifyOnExit: true,
    extras: {
      route_id: 1234
    }
  }).then((success) => {
    console.log("[addGeofence] success");
  }).catch((error) => {
    console.log("[addGeofence] FAILURE: ", error);
  })

  bgGeo.onLocation(function (location) {
    console.log('[location] -', location)
    let coords = location?.coords
    if (!coords || !coords.latitude || !coords.longitude) {
      return false //invalid location
    }
    let {latitude, longitude} = location.coords
  })
  bgGeo.onGeofence(geofence => {
    console.log("[geofence] ", geofence);
    if (geofence.identifier == "LogTripDest") {
      if (geofence.action == "ENTER") {
        console.log(`geofence entered`)
      } else if (geofence.action == "EXIT") {
        // Exiting the danger-zone, we resume geofences-only tracking.
        // BackgroundGeolocation.startGeofences();
        console.log(`geofence exit`)
      }
    }
  })
  bgGeo.stop()
  bgGeo.start()
}

async function stop_log() {
  console.log('Stop log')

}
